require("dotenv").config();
import {ApolloServer, mergeSchemas} from "apollo-server";
import {
    createRateLimitTypeDef,
} from "graphql-rate-limit-directive";

import resolvers from "./resolvers";
import typeDefs from "./schemas";

const schema = mergeSchemas({
    resolvers,
    schemas: [createRateLimitTypeDef(), ...typeDefs],
    // schemaDirectives: {
    //     rateLimit: createRateLimitDirective(),
    // },
});

const context = async ({req}) => {
    const language = req?.headers['x-hasura-lng'] ||
        req?.headers['content-language'];

    // ['ru'] -  default lng
    const [lng] = language?.split('-') || ['ru'];

    return {lng}
};

console.log(process.env.NODE_ENV);

const config: any = {
    schema,
    context,
    playground: process.env.NODE_ENV === "development",
};

const server = new ApolloServer(config);

server.listen({
    port: process.env.PORT || 4000
}).then(({url}) => {
    console.log(`🚀  Server ready at ${url}`);
});
