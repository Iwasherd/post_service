import postsResolvers from "./posts";

const resolvers = {
    ...postsResolvers
};

export default resolvers;
