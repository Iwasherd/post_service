import {ApolloError} from "apollo-server";

import client from "@/utils/client";

import POSTS from "@/services/queries/posts";

const resolvers = {
    Query: {
        postServicePosts: async (_, {}, {}) => {
            try {
                const {post} = await client.request(POSTS);
                return post;
            } catch (e) {
                throw new ApolloError('[postServicePosts]: Service Unavailable', '503')
            }
        }
    },
    PostServicePosts: {
        title: (parent, {}, {lng}) => {
            const currentTranslation = parent?.translations?.find(translate => translate?.locale?.locale === lng);
            if (currentTranslation) {
                return currentTranslation.title
            }
            return null
        },
        content: (parent, {}, {lng}) => {
            const currentTranslation = parent?.translations?.find(translate => translate?.locale?.locale === lng);
            if (currentTranslation) {
                return currentTranslation.content
            }
            return null
        },
    },
};


export default resolvers;
