import {gql} from "apollo-server";

const typeDefs = gql`
    type PostServicePostTranslations @rateLimit {
        title: String
        content: String
        locale: Locale
    }

    type PostServicePosts @rateLimit {
        id: Int
        title: String
        content: String
        translations: [PostServicePostTranslations]
        createdAt: String
        image: String
    }

    type Query @rateLimit(limit: 60, duration: 60) {
        postServicePosts: [PostServicePosts]
    }

    type Locale {
        locale: String
    }
`;

export default typeDefs;
