import {gql} from 'apollo-server';

const LOCALES = gql`
    query locales{
        locales{
            id
            locale
        }
    }
`

export default LOCALES;
