import {gql} from 'apollo-server';

const POST_TRANSLATIONS = gql`
    query post_translations($postId: Int) {
        post_translations(where: {postId: {_eq: $postId}}) {
            content
            id
            title
            postId
            locale {
                locale
            }
        }
    }
`;

export default POST_TRANSLATIONS;
