import {gql} from 'apollo-server';

const POSTS = gql`
    query post {
        post {
            id
            createdAt
            translations {
                content
                id
                title
                locale {
                    locale
                }
            }
        }
    }
`;

export default POSTS;
