import {GraphQLClient} from 'graphql-request';

const client = new GraphQLClient('https://translations.hasura.app/v1/graphql')
client.setHeader('x-hasura-admin-secret', process.env.HASURA_SECRET);

export default client;
